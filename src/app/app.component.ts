import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';


import { HomePage } from '../pages/home/home';
import {NativeAudio} from "@ionic-native/native-audio";
@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any = HomePage;
  // rootPage:any = RandomImagesPage;

  constructor(public platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, public nativeAudio:NativeAudio) {
    // splashScreen.show();
    platform.ready().then(() => {
      this.audioSetting();
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });
  }


  audioSetting(){
    if (this.platform.is('cordova')) {
      this.nativeAudio.preloadSimple('transition_to_dark', 'assets/audio/transition_to_dark.mp3').then(function(){
      });
      this.nativeAudio.preloadSimple('transition_to_light', 'assets/audio/transition_to_light.mp3').then(function(){

      });
    } else {

    }
  }
}

