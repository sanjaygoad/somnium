import { Injectable } from '@angular/core';

import 'rxjs/add/operator/map';

/*
  Generated class for the DataProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
@Injectable()
export class DataProvider {

   public players:Array<any>;
  constructor() {
    this.players=[];
    this.players.push({ name:"Red", color:"red", selected:false, image:"Orbs-Red-iPhone.png"});
    this.players.push({ name:"Blue", color:"blue", selected:false, image:"Orbs-Blue-iPhone.png"});
    this.players.push({ name:"Orange", color:"orange",selected:false,  image:"Orbs-Orange-iPhone.png"});
    this.players.push({ name:"Yellow", color:"yellow", selected:false, image:"Orbs-Yellow-iPhone.png"});
    this.players.push({ name:"Violet", color:"darkviolet", selected:false, image:"Orbs-Purple-iPhone.png"});
    this.players.push({ name:"Green", color:"green",selected:false,  image:"Orbs-Green-iPhone.png"});
  }


}
