import { Component } from '@angular/core';
import {IonicPage, NavController, NavParams, Platform} from 'ionic-angular';
import {DataProvider} from "../../providers/data/data";
import { NativeAudio } from '@ionic-native/native-audio';

@IonicPage()
@Component({
  selector: 'page-random-images',
  templateUrl: 'random-images.html',
})
export class RandomImagesPage {

  players:Array<any>;
  lastNumber:number=-1;
  currentNumber:number=-1;
  toggle:boolean=false;
  dark=false;
  backgroundImage:string;
  intervalFunction:any;
  fadeinPeriod=false;
  constructor(public navCtrl: NavController,
              public platform:Platform,
              public navParams: NavParams,
              public data:DataProvider,
              private nativeAudio: NativeAudio

  ) {
    this.players=this.navParams.get("selectedPlayers");
    let self = this;
    this.playSound("transition_to_light");
    this.changeImage();
    this.changeBg();
    this.intervalFunction = setInterval(function(){
      {
        self.changeBg();
        if(self.backgroundImage === 'dark'){
          self.playSound("transition_to_dark")
        }else{
          self.playSound("transition_to_light")
        }
      }
    }, 1000*10);  // five minute
  }



  playSound(id){
    if (this.platform.is('cordova')) {
      this.nativeAudio.play(id).then(function(){
      });
    }
  }


  changeBackGround(){
    this.dark = !this.dark;
  }

  changeImage(){
    this.toggle=!this.toggle;
    this.generateRandomNumber();
  }

  changeBg(){
    let newImage;
    if(this.backgroundImage==='dark'){
      newImage="light";
    }else{
      newImage="dark";
    }
    this.backgroundImage="";
     setTimeout(()=>{
      this.backgroundImage=newImage;
      this.fadeinPeriod=false;
    },10);
  }

  getEvenImage(){
     if(this.toggle){
       return "assets/images/" + this.players[this.currentNumber].image;
     }else{
       return "assets/images/" + this.players[this.lastNumber].image;
     }

  }
  getOddImage(){

    if(this.toggle){
      return "assets/images/" + this.players[this.lastNumber].image;
    }else{
      return "assets/images/" + this.players[this.currentNumber].image;
    }
  }


  ionViewWillLeave(){
    clearInterval(this.intervalFunction);
  }
  generateRandomNumber(){
    this.lastNumber=this.currentNumber;
   let random = this.random();
    while(this.currentNumber==random){
      random = this.random();
    }

    this.currentNumber = random;
    return random;
  }

  random(){
    return Math.floor((Math.random() * this.players.length) );
  }
}
