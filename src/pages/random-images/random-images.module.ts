import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RandomImagesPage } from './random-images';

@NgModule({
  declarations: [
    RandomImagesPage,
  ],
  imports: [
    IonicPageModule.forChild(RandomImagesPage),
  ],
  exports: [
    RandomImagesPage
  ]
})
export class RandomImagesPageModule {}
