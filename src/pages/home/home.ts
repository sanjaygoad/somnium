import { Component } from '@angular/core';
import {AlertController, NavController, PopoverController} from 'ionic-angular';
import {DataProvider} from "../../providers/data/data";
import {RulesPage} from "../rules/rules";
import {RandomImagesPage} from "../random-images/random-images";

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  allPlayers:Array<any>;
  selectedPlayers:Array<any>=[];
  constructor(public navCtrl: NavController,
              public alertCtrl: AlertController,
              public  data:DataProvider,
              public popoverCtrl: PopoverController) {
    this.allPlayers = data.players;
  }

  selectPlayer(player){
    if(player.selected){
      player.selected=false;
    }else{
      player.selected=true;
    }


    // if(this.selectedPlayers.indexOf(player) > -1){
    //   let index = this.selectedPlayers.indexOf(player);
    //   if (index > -1) {
    //     this.selectedPlayers.splice(index, 1);
    //   }
    // }else{
    //   this.selectedPlayers.push(player);
    // }
    // console.log(this.selectedPlayers);
  }

  rulesPage(){
    this.navCtrl.push(RulesPage,{

    });

    // let popover = this.popoverCtrl.create(RulesPage);
    // popover.present();
  }

  playGame(){
    let selectedPlayers= this.allPlayers.filter((player)=>  player.selected);
    if(selectedPlayers.length<2){
      let alert = this.alertCtrl.create({
        title: "Error",
        subTitle: "Please select two or more players to play",
        buttons: ['OK']
      });
      alert.present();
      return;
    }

    this.navCtrl.push(RandomImagesPage,{
        selectedPlayers:selectedPlayers
    });
  }

  purchase(){
    window.open('http://example.com','_system');
    return false;
  }
}
